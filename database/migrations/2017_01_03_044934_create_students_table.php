<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name',100)->nullable();
            $table->string('email')->unique();
            $table->string('phone');
            $table->text('address');
            $table->string('highest_education');
            $table->enum('gender',['male','female'])->default('male');
            $table->string('image');
            $table->text('bio');
            $table->string('facebook');
            $table->integer('course_id')->unsigned();

            $table->foreign('course_id')->references('id')->on('courses');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('students');
    }
}
