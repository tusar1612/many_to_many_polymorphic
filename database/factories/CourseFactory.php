<?php
$factory->define(App\Course::class, function (Faker\Generator $faker) {


    return [
        'description' => $faker->realText(50,true),
        'title' => $faker->title,
       
        'objective'=>$faker->realText(50,true),
        'start_date'=>$faker->date('y-m-d'),
        'outline' =>$faker->realText(50,true),
        'used_tools' => $faker->realText(50,true),
        
        'end_date'=>$faker->date('y-m-d'),
        'duration'=>$faker->numberBetween(1,3),
        'quarter_id' => $faker->numberBetween(1,5),

    ];
});