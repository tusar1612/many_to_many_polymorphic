<?php
$factory->define(App\Teacher::class, function (Faker\Generator $faker) {


    return [
        'firstName' => $faker->firstName,
        'lastName' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'address'=>$faker->address,
        'mobile' =>$faker->phoneNumber,
        'designation' => $faker->jobTitle,

    ];
});