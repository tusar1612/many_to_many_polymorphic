<?php
$factory->define(App\Trainer::class, function (Faker\Generator $faker) {


    return [
        'name' => $faker->firstName,
        'email' => $faker->unique()->safeEmail,
       
    ];

});