<?php

use Illuminate\Database\Seeder;

class RemarksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


      //  factory(App\Remark::class, 5)->create();

        $Trainer = \App\Trainer::all();
        foreach ($Trainer as $Trainer) {
            $Trainer->remarks()->saveMany(factory(\App\Remark::class, mt_rand(2, 8))->make());
        }

        $student = \App\Student::all();
        foreach ($student as $student) {
            $student->remarks()->saveMany(factory(\App\Remark::class, mt_rand(2, 8))->make());
        }

    }
}
