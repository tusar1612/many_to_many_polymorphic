<?php

use Illuminate\Database\Seeder;

class TaggablesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $courses =\App\Course::all();
        foreach ($courses as $course) {
            $course->tags()->attach($faker->randomElements(App\Tag::pluck('id')->toArray(), mt_rand(2, 4)));
        }

        $trainers =\App\Trainer::all();
        foreach ($trainers as $trainer) {
            $trainer->tags()->attach($faker->randomElements(\App\Tag::pluck('id')->toArray(), mt_rand(2, 4)));
        }

    }
}
