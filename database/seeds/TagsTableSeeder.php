<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $tags = ['PHP 34', 'PHP 35', 'PHP 36', 'WD 30', 'WD 31', 'WD 32'];

        foreach ($tags as $tag) {
            DB::table('tags')->insert([
                'name' => $tag,
                'created_by' => $faker->randomElement(App\Course::pluck('id')->toArray()),
                'created_at' => $faker->dateTimeThisYear(),
                'updated_at' => $faker->dateTimeThisMonth(),
            ]);
        }

    }
}
