<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quarter extends Model
{

    public function course(){

       return $this->hasMany('App\Course');
    }

    public function students(){

        return $this->hasManyThrough('App\Student','App\Course');
    }
}
