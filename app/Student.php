<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{

    
    public function remarks(){
        return $this->morphMany('App\Remark','remarkable');
    }
    
    public function quarter(){
        
        
        return $this->belongsToMany('App\Quarter');
    }
}
