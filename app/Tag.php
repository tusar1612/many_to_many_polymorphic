<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function courses() {
        return $this->morphedByMany('App\Course', 'taggable');
    }

    public function trainers()     {
        return $this->morphedByMany('App\Trainer', 'taggable');
    }

}
